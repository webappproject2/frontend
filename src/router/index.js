import Vue from 'vue'
import Router from 'vue-router'
import SignIn from '@/components/SignIn'
import SignUp from '@/components/SignUp'
import AlbumsIndex from '@/components/albums/Index.vue'
import AlbumsShow from '@/components/albums/Show.vue'
import AlbumsNew from '@/components/albums/New.vue'
import PostsNew from '@/components/posts/New.vue'
import store from '../store.js'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      component: AlbumsIndex
    },
    {
      path: '/sign_in',
      name: 'Users.sign_in',
      component: SignIn
    },
    {
      path: '/albums',
      name: 'Albums.index',
      component: AlbumsIndex
    },
    {
      path: '/sign_up',
      name: 'Users.sign_up',
      component: SignUp,
      beforeEnter: checkAuth
    },
    {
      path: '/posts/new',
      name: 'Posts.new',
      component: PostsNew,
      beforeEnter: checkAuth
    },
    {
      path: '/albums/new',
      name: 'Albums.new',
      component: AlbumsNew,
      beforeEnter: checkAuth
    },
    {
      path: '/albums/:albumID',
      name: 'Albums.show',
      component: AlbumsShow
    }
  ]
})

function checkAuth (to, from, next) {
  if (!store.state.auth) {
    next('/')
  } else {
    next()
  }
}
