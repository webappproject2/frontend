// file-upload.service.js
import Vue from 'vue'

function getAllPublicAlbums () {
  const url = '/api/album/public'

  return Vue.$http.get(url).then((response) => {
    return response.data
  }).catch((response) => {
    return null
  })
}

export { getAllPublicAlbums }
