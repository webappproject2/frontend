// file-upload.service.js
import Vue from 'vue'
import store from '../store'

function login (username, password, callback) {
  const url = '/api/login'

  var auth = {
    username: username,
    password: password
  }
  return Vue.$http.post(url, auth).then((response) => {
    store.dispatch('login')
    callback(response.data)
  }).catch((response) => {
    store.dispatch('logout')
  })
}

export { login }
