// file-upload.service.js
import Vue from 'vue'
import store from '../store'

function logout (callback) {
  const url = '/api/logout'

  return Vue.$http.delete(url).then((response) => {
    store.dispatch('logout')
    callback(response.data)
  }).catch((response) => {
    store.dispatch('logout')
  })
}

export { logout }
