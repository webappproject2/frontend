// file-upload.service.js
import Vue from 'vue'

function getTags (imageID) {
  const url = `/api/image/${imageID}/tags`

  return Vue.$http.get(url).then((response) => {
    // console.log('this is response ', response)
    return response.data
  }).catch((response) => {
    return []
  })
}

export { getTags }
