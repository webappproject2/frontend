// file-upload.service.js
import Vue from 'vue'

function signUp (username, password, firstname, lastname) {
  const url = '/api/user/create'

  const auth = {
    username: username,
    password: password,
    firstname: firstname,
    lastname: lastname
  }
  return Vue.$http.post(url, auth).then((response) => {
    return true
  }).catch((response) => {
    return false
  })
}

export { signUp }
