// file-upload.service.js
import Vue from 'vue'

function upload (albumID, formData) {
  const baseUrl = 'http://localhost:3000'
  const url = `/api/album/${albumID}/upload`
  return Vue.$http.post(url, formData).then((response) => {
    const {data} = response
    data.url = `${baseUrl}${data.url}`
    return data
  })
}

export { upload }
