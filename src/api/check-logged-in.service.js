// file-upload.service.js
import Vue from 'vue'

function checkLoggedIn () {
  const url = '/api/user/whoami'

  return Vue.$http.get(url).then((response) => {
    return response.data
  }).catch((response) => {
    return null
  })
}

export { checkLoggedIn }
