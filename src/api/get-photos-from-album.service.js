// file-upload.service.js
import Vue from 'vue'

function getAllPhotos (albumID) {
  const url = `/api/album/${albumID}`

  return Vue.$http.get(url).then((response) => {
    // console.log('hello', response)
    return response.data
  }).catch((response) => {
    return null
  })
}

export { getAllPhotos }
