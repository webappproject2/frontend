// file-upload.service.js
import Vue from 'vue'

function createAlbum (name, isPublic) {
  const baseUrl = 'http://localhost:3000'
  const url = '/api/album/create'

  var auth = {
    name: name,
    isPublic: isPublic
  }
  return Vue.$http.post(url, auth).then((response) => {
    const {data} = response
    data.url = `${baseUrl}${data.url}`
    return data
  })
}

export { createAlbum }
