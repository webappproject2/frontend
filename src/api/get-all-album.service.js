// file-upload.service.js
import Vue from 'vue'

function getAllAlbums () {
  const url = '/api/album'

  return Vue.$http.get(url).then((response) => {
    return response.data
  }).catch((response) => {
    return null
  })
}

export { getAllAlbums }
