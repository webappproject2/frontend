# ICCS340 Project 2 Front-end

> Trung Do Dinh (Sky) 5780182

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```
